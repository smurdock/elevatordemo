import com.seanmurdock.Elevator.model.ElevatorRequest;
import com.seanmurdock.Elevator.util.ElevatorWorker;

import java.util.SortedSet;

/**
 * Created by Administrator on 2/24/2015.
 */
public class TestElevatorUp {

    //be sure to test that once it reaches the top floor, it considers if the user entered a floor higher
    //(this means basically at this point everyone else is off of the elevator any way)

    public static void main (String[] args) throws Exception {
        System.out.println("Testing Same Floor");
        testSameStop(1);
    }

    private static void testSameStop(Integer startingFloor) throws Exception{
        ElevatorWorker worker = new ElevatorWorker();
        System.out.println("Elevator is at floor: "+worker.getCurrentFloor());
        ElevatorRequest pickUpRequest = new ElevatorRequest(startingFloor,1);//pick me up on nth floor, going up
        worker.addRequest(pickUpRequest);//pick me up at the first floor (going up)
        System.out.println("Requested pick up from floor: "+pickUpRequest.getFloor());
        SortedSet<Integer> stops =worker.up(startingFloor);//starting at the first floor

        for (Integer stop:stops){
            System.out.println("Stopping at floor: "+stop);
            Thread.sleep(1000);
        }

    }


}
