package com.seanmurdock.Elevator.model;

import com.google.gwt.thirdparty.guava.common.hash.HashCode;

/**
 * Created by Administrator on 2/24/2015.
 */
public class ElevatorRequest implements Comparable {

    private Integer floor;
    private Integer direction;

    public ElevatorRequest(Integer floor,Integer direction){
        this.floor=floor;
        this.direction=direction;
    }

    public Integer getFloor(){
        return floor;
    }

    public void setFloor(Integer floor){
        this.floor=floor;
    }

    public Integer getDirection(){
        return direction;
    }

    public void setDirection(Integer direction){
        this.direction=direction;
    }


    @Override
    public int compareTo(Object o) {
        ElevatorRequest request = (ElevatorRequest) o;
        return this.floor-request.floor;
    }

    @Override public int hashCode(){
        return floor;
    }

    @Override public boolean equals(Object o){
        ElevatorRequest request = (ElevatorRequest) o;
        if (null!=floor && null!=direction && floor==request.floor && direction==request.direction){
            return true;
        }

        else{
            return false;
        }

    }
}
