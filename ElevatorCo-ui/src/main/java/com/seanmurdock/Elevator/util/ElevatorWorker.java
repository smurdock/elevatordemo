package com.seanmurdock.Elevator.util;

import com.seanmurdock.Elevator.model.ElevatorRequest;

import java.util.*;

/**
 * Created by Administrator on 2/24/2015.
 */

public class ElevatorWorker {

    private ElevatorRequest currentFloor=new ElevatorRequest(1,0);

    public SortedSet<ElevatorRequest> elevatorRequests = new TreeSet<ElevatorRequest>();

    public void setCurrentFloor(Integer floorNumber){
        this.currentFloor=new ElevatorRequest(floorNumber,0);
    }

    public Integer getCurrentFloor(){
        return currentFloor.getFloor();
    }

    public SortedSet<Integer> up(Integer floorNumber){
        ElevatorRequest startingFloor = new ElevatorRequest(floorNumber,0);
        SortedSet<Integer> stops = new TreeSet<Integer>();//put the stops that are made in a set for later use
        stops.add(startingFloor.getFloor());
        if(elevatorRequests.size()>0){//we have requests
            SortedSet<ElevatorRequest> route = elevatorRequests.headSet(startingFloor);//everything above the current floor
            for (ElevatorRequest request:route){
                if(request.getDirection()>0 || request.getDirection()==0){//only stop for upward requests (or people on the elevator)
                    stops.add(request.getFloor());
                    elevatorRequests.remove(request);//remove from future
                }
            }
        }

        return stops;//set of floors
    }

    public SortedSet<Integer> down(ElevatorRequest startingFloor){
        SortedSet<Integer> stops = new TreeSet<Integer>();//put the stops in a set
        stops.add(startingFloor.getFloor());

        if(elevatorRequests.size()>0){
            SortedSet<ElevatorRequest> route = elevatorRequests.tailSet(startingFloor);//everything below the current floor
            for (ElevatorRequest request:route){
                if(request.getDirection()<0 || request.getDirection()==0){//only stop for downward requests (or people in the elevator)
                    stops.add(request.getFloor());
                    elevatorRequests.remove(request);//remove from future
                }
            }
        }

        return stops;
    }

    public void addRequest(ElevatorRequest request){
        elevatorRequests.add(request);
    }
}
